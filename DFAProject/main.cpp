#include <iostream>
#include <fstream>
#include "InitialStateException.h"
#include "AutomatonException.h"
#include "DeterministicFiniteAutomaton.h"
#include "AbstractFiniteAutomaton.h"

using namespace std;

AbstractFiniteAutomaton *getNewAutomaton(const int &automatonType) {
  switch (automatonType) {
    case 1:
      return new DeterministicFiniteAutomaton<int>();
    case 2:
      return new DeterministicFiniteAutomaton<char>();
    default:
      throw AutomatonException("Unsupported alphabet type!");
  }
}

bool isFile() {
  cout << "Read automaton from file (Y/N)?" << endl;
  char input;
  cin >> input;
  input = toupper(input);
  
  return (input == 'Y');
}

void handleOperations(const int &automatonType, AbstractFiniteAutomaton *automaton) {
  while (true) {
    cout << "\nSELECT OPERATION TYPE:" << endl;
    cout << " >> 0: print" << endl;
    cout << " >> 1: check word" << endl;
    cout << " >> 2: (&) intersection" << endl;
    cout << " >> 3: (|) union" << endl;
    cout << " >> 4: (^) complementation" << endl;
    cout << " >> 5: exit" << endl;
    
    int operationType;
    cin >> operationType;
    
    AbstractFiniteAutomaton *newAutomaton;
    AbstractFiniteAutomaton *resultAutomaton;
    char *word = new char[100];
    bool isRecognizable;
    fstream inputFile;
    switch (operationType) {
      case 0:
        cout << "Printing automaton..." << endl;
        cout << automaton << endl;
        cout << "\n==========" << " PRINT FINISHED " << "==========" << endl;
        break;
      case 1:
        cout << "Enter a word: "<<endl;
        cin >> word;
        isRecognizable = automaton->recognizeWord(word);
        cout << "Can automaton recognize "<< "\""<< word << "\"? " <<(isRecognizable ? "true\n" : "false\n");
        delete[] word;
        break;
      case 2:
        newAutomaton = getNewAutomaton(automatonType);
        if (isFile()) {
          cout << "Please enter path to file: " << endl;
          char *inputFileName = new char[100];
          cin >> inputFileName;
          inputFile.open(inputFileName, ios::in);
          delete [] inputFileName;
          if (!inputFile.is_open()) {
            cout << "Input file doesn't exist!";
            return;
          }
          cout << "Reading from file..." << endl;
          int newAutomatonType;
          inputFile >> newAutomatonType;
          if (newAutomatonType != automatonType) {
            cout << newAutomatonType;
            cout << automatonType;
            throw AutomatonException("Cannot perform operations on different types of automatons!");
          }
          inputFile >> newAutomaton;
          inputFile.close();
        } else {
          cout << "Please enter automaton data:" << endl;
          cin >> newAutomaton;
        }
    
        resultAutomaton = (*automaton & *newAutomaton);
        delete automaton;
        delete newAutomaton;
        
        automaton = resultAutomaton;
        cout << "Intersection completed.\n";
        cout << automaton;
        break;
      case 3:
        newAutomaton = getNewAutomaton(automatonType);
        if (isFile()) {
          cout << "Please enter path to file: " << endl;
          char *inputFileName = new char[100];
          cin >> inputFileName;
          inputFile.open(inputFileName, ios::in);
          delete [] inputFileName;
          if (!inputFile.is_open()) {
            cout << "Input file doesn't exist!";
            return;
          }
          cout << "Reading from file..." << endl;
          int newAutomatonType;
          inputFile >> newAutomatonType;
          if (newAutomatonType != automatonType) {
            cout << newAutomatonType;
            cout << automatonType;
            throw AutomatonException("Cannot perform operations on different types of automatons!");
          }
          inputFile >> newAutomaton;
          inputFile.close();
        } else {
          cout << "Please enter automaton data:" << endl;
          cin >> newAutomaton;
        }
        
        cout << "Union stared." << endl;
        resultAutomaton = (*automaton | *newAutomaton);
        delete automaton;
        delete newAutomaton;
        
        cout << resultAutomaton << endl;
        
        automaton = resultAutomaton;
        break;
      case 4:
        cout << "Complementation started." << endl;
        automaton = (~*automaton);
        cout << automaton;
        break;
      case 5:
        return;
      default:
        cout << "Invalid operation!" << endl;
        return;
    }
  }
}

int main(int argc, char **argv) {
  ifstream inputFile;
  int automatonType;
  try {
    bool readFile = isFile();
    if (readFile) {
      cout << "Please enter path to file: " << endl;
      char *inputFileName = new char[100];
      cin >> inputFileName;
      inputFile.open(inputFileName, ios::in);
      delete [] inputFileName;
      if (!inputFile.is_open()) {
        cout << "Input file doesn't exist!";
        return 1;
      }
      cout << "Reading from file..." << endl;
      inputFile >> automatonType;
    } else {
      cout << "Enter automaton type:" << endl;
      cin >> automatonType;
    }
    
    AbstractFiniteAutomaton *automaton = getNewAutomaton(automatonType);
    
    if (readFile) {
      inputFile >> automaton;
    } else {
      cin >> automaton;
    }
    
    cout << "\n==========" << " Reading automaton finished " << "==========" << endl;
    
    handleOperations(automatonType, automaton);
  } catch (AutomatonException &exception) {
    cout << "AutomatonException occurred: ";
    exception.rep(cout);
    return 1;
  } catch (AutomatonStateException &automatonStateException) {
    cout << "AutomatonStateException occurred:";
    automatonStateException.rep(cout);
  } catch (InitialStateException &initialStateException) {
    cout << "InitialStateException occurred: ";
    initialStateException.rep(cout);
  }
  
  return 0;
}