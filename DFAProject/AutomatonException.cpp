#include "AutomatonException.h"

AutomatonException::AutomatonException(const char* errorMessage):
    std::exception(), errorMessage(errorMessage) { }

std::ostream& AutomatonException::rep(std::ostream & out) {
  out << errorMessage;
  return out;
}