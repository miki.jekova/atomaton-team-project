#ifndef DFAPROJECT_AUTOMATONEXCEPTION_H
#define DFAPROJECT_AUTOMATONEXCEPTION_H
#pragma once

#include <iostream>
#include <exception>

class AutomatonException : public std::exception {
public:
    /**
     * Constructor to initialize the exception.
     *
     * @param stateName Error message of the exception.
     */
    explicit AutomatonException(const char *);
    
    /**
     * Prints the exception to the console.
     *
     * @return output stream that writes to the console.
     */
    std::ostream &rep(std::ostream &);

private:
    const char *errorMessage;
};

#endif