#include "InitialStateException.h"
#include <iostream>

InitialStateException::InitialStateException(const char* currentState, const char* initialState):
    std::exception(), currentStateName(currentState), initialStateName(initialState) { }

std::ostream& InitialStateException::rep(std::ostream & out) {
  out << "Automaton already has an initial state: "<< initialStateName << " Cannot set \"" << currentStateName << "\" as initial state.";
  return out;
};