#ifndef DFAPROJECT_INITIALSTATEEXCEPTION_H
#define DFAPROJECT_INITIALSTATEEXCEPTION_H
#pragma once
#include <iostream>
#include <exception>

class InitialStateException: public std::exception {
private:
    const char* currentStateName;
    const char* initialStateName;
public:
    /**
     * Constructor to initialize the exception.
     *
     * @param initialStateName Current initial state of the automaton.
     * @param currentStateName Name of State object that cannot be set to initial.
     */
    InitialStateException(const char*, const char*);
    std::ostream& rep(std::ostream&);
};

#endif
