#ifndef DFAPROJECT_STATE_H
#define DFAPROJECT_STATE_H
#pragma once
#include <iostream>

class State {
public:
    /**
     * Initialize an default State with an empty name.
     */
    State();
    
    /**
     * Destructor of the State object.
     */
    ~State();
    
    /**
     * Initialize a State object.
     * @param name Name of the new State
     */
    State(char *name);
    
    /**
     * Initializes a copy of a State object.
     */
    State(const State &);
    
    /**
     * Predefinition of assignment operator
     *
     * @return reference to the state.
     */
    State &operator=(const State &);
    
    /**
     * @return name of the state.
     */
    char *getName() const;
    
    /**
     * Sets name of the state;
     *
     * @param name Name of the state.
     */
    void setName(char *name);
    
    /**
     * @return true if state is initial, false if state is not initial.
     */
    bool getIsInitial() const;
    
    /**
     * Sets true if state is initial and false if it is not initial.
     *
     * @param isInitial Value if state is initial.
     */
    void setIsInitial(bool isInitial);
    
    /**
     * @return true if state is finite, false if state is not finite.
     */
    bool getIsFinite() const;
    
    /**
     * Sets true if state is finite and false if it is not finite.
     *
     * @param isFinite Value if state is initial.
     */
    void setIsFinite(bool isFinite);


private:
    char* name;
    bool isInitial;
    bool isFinite;
};

#endif