#include "AutomatonStateException.h"

AutomatonStateException::AutomatonStateException(const char* stateName):
    std::exception(), stateName(stateName) { }

std::ostream& AutomatonStateException::rep(std::ostream & out) {
  out << "Cannot find state \"" << stateName << "\" . Please enter an existing state.\n";
  return out;
};