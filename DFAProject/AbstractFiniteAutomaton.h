#include <iostream>
#include "State.h"

#ifndef DFAPROJECT_ABSTRACTFINITEAUTOMATON_H
#define DFAPROJECT_ABSTRACTFINITEAUTOMATON_H

class AbstractFiniteAutomaton {
public:
    /**
     * @param in input stream to read from
     * @param rhs automaton to write to
     * @return input stream
     */
    friend std::istream &operator>>(std::istream &, AbstractFiniteAutomaton *);
    
    /**
     * @param out output stream to write to
     * @param rhs automaton to read from
     * @return output stream
     */
    friend std::ostream &operator<<(std::ostream &, AbstractFiniteAutomaton *);
    
    /**
     * Returns an input stream for the automaton.
     *
     * @param in Input stream to read from.
     * @return input stream for the automaton.
     */
    virtual std::istream &ext(std::istream &in) = 0;
    
    /**
     * Returns an output stream for the automaton.
     *
     * @param out Output stream to write to.
     * @return output stream for the automaton.
     */
    virtual std::ostream &print(std::ostream &out) const = 0;
    
    /**
     * Check if a word is in the language of the automaton.
     *
     * @param word The word to read. Must be a dynamic char array.
     * @return true if the word is in the language of the automaton, false if it is unrecognizable.
     */
    virtual bool recognizeWord(char *) = 0;
    
    /**
     * Get the union of two automatons.
     *
     * @param rhs Pointer to an AbstractFiniteAutomaton object.
     * @return new automaton, result of the union.
     */
    virtual AbstractFiniteAutomaton *operator|(AbstractFiniteAutomaton &) = 0;
    
    /**
     * Get the intersection of two automatons.
     *
     * @param rhs Pointer to an AbstractFiniteAutomaton object.
     * @return new automaton, result of the intersection.
     */
    virtual AbstractFiniteAutomaton *operator&(AbstractFiniteAutomaton &) = 0;
    
    /**
     * Get the complementation of the automaton.
     *
     * @return new automaton, result of the complementation.
     */
    virtual AbstractFiniteAutomaton *operator~() = 0;
    
    /**
     * Destructor of an AbstractFiniteAutomaton object.
     */
    virtual ~AbstractFiniteAutomaton() = default;
    
    /**
     * @return count of the states of the automaton
     */
    virtual unsigned int getStateCount() const = 0;
    
    /**
     * @return states of the automaton.
     */
    virtual State *getQ() const = 0;
    
    /**
     * @return transition table of the automaton.
     */
    virtual State **getTransitionTable() const = 0;
};

std::istream &operator>>(std::istream &in, AbstractFiniteAutomaton *rhs) {
  return rhs->ext(in);
}

std::ostream &operator<<(std::ostream &out, AbstractFiniteAutomaton *rhs) {
  return rhs->print(out);
}

#endif
