#ifndef DFAPROJECT_AUTOMATONSTATEEXCEPTION_H
#define DFAPROJECT_AUTOMATONSTATEEXCEPTION_H
#pragma once

#include <iostream>
#include <exception>

class AutomatonStateException : public std::exception {
public:
    /**
     * Constructor to initialize the exception.
     *
     * @param stateName Name of the state that caused the exception.
     */
    explicit AutomatonStateException(const char *);
    
    /**
     * Prints the exception to the console.
     *
     * @return output stream that writes to the console.
     */
    std::ostream &rep(std::ostream &);

private:
    const char *stateName;
};

#endif