#include "State.h"
#include <cstring>

State::State() {
  name = nullptr;
  isInitial = false;
  isFinite = false;
}

State::State(char *name) {
  this->name = new char[strlen(name)];
  strcpy(this->name, name);
  isInitial = false;
  isFinite = false;
}

State::~State() {
  delete[] name;
  isFinite = false;
}

State::State(const State &rhs) {
  name = new char[100];
  
  for (int i = 0; i < strlen(rhs.name); i++) {
    name[i] = rhs.name[i];
  }
  isInitial = rhs.isInitial;
  isFinite = rhs.isFinite;
}

State &State::operator=(const State &rhs) {
  if (this != &rhs) {
    if (name != nullptr) {
      delete[] name;
    }
    name = new char[100];
    strcpy(name, rhs.name);
    name[strlen(rhs.name)] = '\0';
    isInitial = rhs.isInitial;
    isFinite = rhs.isFinite;
  }
  
  return *this;
}

char *State::getName() const {
  return name;
}

void State::setName(char *name) {
  if (this->name != nullptr) {
    delete[] this->name;
  }
  this->name = new char[100];
  strcpy(this->name, name);
}

bool State::getIsInitial() const {
  return isInitial;
}

void State::setIsInitial(bool isInitial) {
  this->isInitial = isInitial;
}

bool State::getIsFinite() const {
  return isFinite;
}

void State::setIsFinite(bool isFinite) {
  this->isFinite = isFinite;
}
