#ifndef DetermineFiniteAutomaton_h
#define DetermineFiniteAutomaton_h
#pragma once

#include <iostream>

#include <cstring>

#include <string>

#include "State.h"

#include "AutomatonStateException.h"

#include "AutomatonException.h"

#include "AbstractFiniteAutomaton.h"

template<typename T>
class DeterministicFiniteAutomaton : public AbstractFiniteAutomaton {
public:
    DeterministicFiniteAutomaton<T>() {
      Q = nullptr;
      stateCount = 0;
      finiteStatesCount = 0;
      alphabetSize = 0;
      transitionTable = nullptr;
      alphabet = nullptr;
    };
    
    DeterministicFiniteAutomaton(State *q, unsigned int stateCount, unsigned int finiteStatesCount,
                                 unsigned int alphabetSize, State **transitionTable, T *alphabet) : Q(q),
                                                                                                stateCount(stateCount),
                                                                                                finiteStatesCount(
                                                                                                    finiteStatesCount),
                                                                                                alphabetSize(
                                                                                                    alphabetSize),
                                                                                                transitionTable(
                                                                                                    transitionTable),
                                                                                                alphabet(alphabet) {}
    
    DeterministicFiniteAutomaton(const DeterministicFiniteAutomaton &rhs) {
      stateCount = rhs.stateCount;
      for (int i = 0; i < stateCount; i++) {
        Q[i] = rhs.Q[i];
      }
      Q = rhs.setQ;
      finiteStatesCount = rhs.finiteStatesCount;
      alphabetSize = rhs.alphabetSize;
      transitionTable = rhs.setTransitionTable;
      alphabet = rhs.setAlphabet;
    }
    
    ~DeterministicFiniteAutomaton() override {
      delete[] Q;
      for (int i = 0; i < stateCount; i++) {
        delete[] transitionTable[i];
      }
      
      delete[] alphabet;
    };
    
    T *getAlphabet() {
      return alphabet;
    };
    
    DeterministicFiniteAutomaton &operator=(const DeterministicFiniteAutomaton &rhs) {
      if (this != &rhs) {
        if (Q != nullptr) {
          delete[] Q;
        }
        
        if (transitionTable != nullptr) {
          for (int i = 0; i < stateCount; i++) {
            delete[] transitionTable[i];
          }
        }
        
        stateCount = rhs.stateCount;
        
        for (int i = 0; i < stateCount; i++) {
          Q[i] = rhs.Q[i];
        }
        
        finiteStatesCount = rhs.finiteStatesCount;
        alphabetSize = rhs.alphabetSize;
        for (int row = 0; row < stateCount; row++) {
          for (int col = 0; col < alphabetSize; col++) {
            transitionTable[row][col] = rhs.transitionTable[row][col];
          }
        }
        
        for (int i = 0; i < alphabetSize; i++) {
          alphabet[i] = rhs.alphabet[i];
        }
      }
      return *this;
    };
    
    /**
     * Change the initial state of the automaton.
     *
     * @param newInitialName the new name of the automaton. Must be a char array.
     */
    void changeInitialState(char *newInitialName) {
      getStateByName(newInitialName).setIsInitial(true);
      
      for (int i = 0; i < getStateCount(); i++) {
        if (strcmp(Q[i].getName(), newInitialName) != 0 && Q[i].getIsInitial()) {
          std::cout << "Current initial state is: " << Q[i].getName();
          
          Q[i].setIsInitial(false);
        }
      }
    };
    
    /**
     * Reads the number of states and the state names and writes them in the automaton.
     *
     * @param in input stream to read the data
     */
    void readStates(std::istream &in) {
      /* Set number of states */
      std::cout << "Enter number of states:" << std::endl;
      int statesCount;
      in >> statesCount;
      setStateCount(statesCount);
      Q = new State[statesCount];
      char *tempName = new char[100];
      
      for (int i = 1; i <= getStateCount(); i++) {
        std::cout << "Enter state number [" << i << "]:" << std::endl;
        in >> tempName;
        State state = State(tempName);
        Q[i - 1] = state;
      }
      
      delete[] tempName;
    }
    
    /**
     * Reads the number of alphabet symbols and the alphabet and writes them to the automaton.
     * @param in input stream to read the data from.
     */
    void readAlphabet(std::istream &in) {
      std::cout << "Enter number of alphabet symbols:" << std::endl;
      in >> alphabetSize;
      
      alphabet = new T[alphabetSize];
      for (int i = 0; i < alphabetSize; i++) {
        std::cout << "Enter symbol [" << i + 1 << "]:" << std::endl;
        in >> alphabet[i];
      }
    };
    
    /**
     * Reads the transitions and writes them to the transition table.
     *
     * @param in input stream to read the data from.
     */
    void readTransitionTable(std::istream &in) {
      /* Set transition table */
      transitionTable = new State *[stateCount];
      char *tempName = new char[100];
      for (int row = 0; row < stateCount; row++) {
        transitionTable[row] = new State[alphabetSize];
        for (int col = 0; col < alphabetSize; col++) {
          std::cout << "Please enter (" << Q[row].getName() << ", " << alphabet[col] << ") -> ";
          in >> tempName;
          transitionTable[row][col] = getStateByName(tempName);
        }
      }
      
      delete[] tempName;
    }
    
    /**
     * Reads the initial state and sets it to the automaton.
     *
     * @param in input stream to read the data from.
     */
    void readInitialState(std::istream &in) {
      char *tempName = new char[100];
      std::cout << "Please enter initial state: " << std::endl;
      in >> tempName;
      changeInitialState(tempName);
      
      delete[] tempName;
    }
    
    /**
     * Reads the number of final states and sets all of them to the automaton.
     *
     * @param in input stream to read the data from.
     */
    void readFinalStates(std::istream &in) {
      std::cout << "Please enter number of finite states:" << std::endl;
      in >> finiteStatesCount;
      
      char *tempName = new char[100];
      for (int i = 0; i < finiteStatesCount; i++) {
        std::cout << "Please enter finite state [" << i + 1 << "]:" << std::endl;
        in >> tempName;
        getStateByName(tempName).setIsFinite(true);
      }
      delete[] tempName;
    }
    
    std::istream &ext(std::istream &in) {
      readStates(in);
      
      readAlphabet(in);
      
      readTransitionTable(in);
      
      readInitialState(in);
      
      readFinalStates(in);
      
      return in;
    };
    
    /**
     * Sets the state count of the automaton.
     *
     * @param automatonStateCount Number of states of the automaton.
     */
    void setStateCount(unsigned int automatonStateCount) {
      stateCount = automatonStateCount;
    };
    
    /**
     * Sets the alphabet size of the automaton.
     *
     * @param alphabetSize Number of alphabet symbols.
     */
    void setAlphabetSize(unsigned int alphabetSize) {
      this->alphabetSize = alphabetSize;
    }
    
    /**
     * Sets the alphabet of the automaton.
     *
     * @param alphabet The alphabet of the automaton.
     */
    void setAlphabet(T *alphabet) {
      if (this->alphabet != nullptr) {
        delete[] alphabet;
      }
      this->alphabet = new T[100];
      memcpy(this->alphabet, alphabet, alphabetSize * sizeof(T));
    }
    
    /**
     * Sets the states of the automaton.
     *
     * @param newQ Dynamic array of the states of the automaton.
     */
    void setQ(State *newQ) {
      if (this->Q != nullptr) {
        delete[] Q;
      }
      this->Q = new State[this->stateCount];
      memcpy(this->Q, newQ, this->stateCount * sizeof(State));
    };
    
    /**
     * Sets the count of the finite states of the automaton.
     *
     * @param finiteStatesCount Number of the finite states count.
     */
    void setFiniteStateCount(unsigned int finiteStatesCount) {
      this->finiteStatesCount = finiteStatesCount;
    }
    
    /**
     * Sets the transition table of the automaton.
     *
     * @param newTransitionTable Two-dimentional array of the transitions.
     */
    void setTransitionTable(State **newTransitionTable) {
      this->transitionTable = new State *[stateCount];
      for (int i = 0; i < stateCount; i++) {
        this->transitionTable[i] = new State[alphabetSize];
        for (int j = 0; j < alphabetSize; j++) {
          this->transitionTable[i][j] = newTransitionTable[i][j];
        }
      }
    }
    
    State *getQ() const override {
      return Q;
    };
    
    unsigned int getStateCount() const override {
      return stateCount;
    };
    
    /**
     * @return count of the finite states of the automaton.
     */
    unsigned int getFiniteStatesCount() const {
      return finiteStatesCount;
    };
    
    /**
     * @return size of the alphabet of the automaton.
     */
    unsigned int getAlphabetSize() const {
      return alphabetSize;
    };
    
    State **getTransitionTable() const override {
      return transitionTable;
    };
    
    /**
     * Returns a state by a given name.
     *
     * @param stateName Name of the state to return.
     * @return state by the given name.
     * @throws AutomatonStateException if the state is not a part of the automaton.
     */
    State &getStateByName(char *stateName) const {
      for (int i = 0; i < getStateCount(); i++) {
        if (strcmp(Q[i].getName(), stateName) == 0) {
          return Q[i];
        }
      }
      
      throw AutomatonStateException(stateName);
    };
    
    bool recognizeWord(char *word) override {
      int alphabetCounter = -1;
      int stateCounter = 0;
      char *currentStateName = new char[100];
      State currentState;
      
      for (int i = 0; i < stateCount; i++) {
        if (Q[i].getIsInitial()) {
          strcpy(currentStateName, Q[i].getName());
        }
      }
      
      for (int letter = 0; letter < strlen(word); letter++) {
        for (int i = 0; i < alphabetSize; i++) {
          if (word[letter] == alphabet[i]) {
            alphabetCounter = i;
            break;
          }
        }
        if (alphabetCounter == -1) {
          return false;
        }
        
        for (int i = 0; i < stateCount; i++) {
          if (strcmp(Q[i].getName(), currentStateName) == 0) {
            stateCounter = i;
          }
        }
        strcpy(currentStateName, transitionTable[stateCounter][alphabetCounter].getName());
        alphabetCounter = -1;
      }
      currentState = State(getStateByName(currentStateName));
      delete[] currentStateName;
      return currentState.getIsFinite();
    }
    
    AbstractFiniteAutomaton *operator|(AbstractFiniteAutomaton &rhs) override {
      const unsigned int newStateCount = this->stateCount * rhs.getStateCount();
      State *q = new State[newStateCount];
      unsigned int newFiniteStatesCount = 0;
      State **newTransitionTable = new State *[newStateCount];
      DeterministicFiniteAutomaton<T> *newAutomaton = new DeterministicFiniteAutomaton<T>();
      
      newAutomaton->setStateCount(newStateCount);
      newAutomaton->setAlphabetSize(alphabetSize);
      newAutomaton->setAlphabet(alphabet);
      
      // Set new states
      unsigned int newStateIdx = 0;
      for (int lhsStateIdx = 0; lhsStateIdx < this->stateCount; lhsStateIdx++) {
        for (int rhsStateIdx = 0; rhsStateIdx < rhs.getStateCount(); rhsStateIdx++) {
          char newName[100];
          newName[0] = '\0';
          strcat(newName, this->Q[lhsStateIdx].getName());
          strcat(newName, ",");
          strcat(newName, rhs.getQ()[rhsStateIdx].getName());
          
          q[newStateIdx].setName(newName);
          bool isNewStateFinite = this->Q[lhsStateIdx].getIsFinite() || rhs.getQ()[rhsStateIdx].getIsFinite();
          q[newStateIdx].setIsFinite(isNewStateFinite);
          newFiniteStatesCount += isNewStateFinite;
          q[newStateIdx].setIsInitial(this->Q[lhsStateIdx].getIsInitial() && rhs.getQ()[rhsStateIdx].getIsInitial());
          
          newStateIdx++;
        }
      }
      newAutomaton->setQ(q);
      newAutomaton->setFiniteStateCount(newFiniteStatesCount);
      
      // Set new transitions
      newStateIdx = 0;
      for (int lhsStateIdx = 0; lhsStateIdx < this->stateCount; lhsStateIdx++) {
        for (int rhsStateIdx = 0; rhsStateIdx < rhs.getStateCount(); rhsStateIdx++) {
          newTransitionTable[newStateIdx] = new State[alphabetSize];
          for (int alphabetIdx = 0; alphabetIdx < alphabetSize; alphabetIdx++) {
            State lhsNext = this->transitionTable[lhsStateIdx][alphabetIdx];
            State rhsNext = rhs.getTransitionTable()[rhsStateIdx][alphabetIdx];
            char nextStateName[100];
            strcpy(nextStateName, lhsNext.getName());
            strcat(nextStateName, ",");
            strcat(nextStateName, rhsNext.getName());
            
            newTransitionTable[newStateIdx][alphabetIdx] = newAutomaton->getStateByName(nextStateName);
          }
          
          newStateIdx++;
        }
      }
      newAutomaton->setTransitionTable(newTransitionTable);
      
      return newAutomaton;
    }
    
    // Intersection
    AbstractFiniteAutomaton *operator&(AbstractFiniteAutomaton &rhs) override {
      DeterministicFiniteAutomaton<T> *newAutomaton = new DeterministicFiniteAutomaton<T>();
      const unsigned int newStateCount = this->stateCount * rhs.getStateCount();
      State *q = new State[newStateCount];
      unsigned int newFiniteStatesCount = 0;
      State **newTransitionTable = new State *[newStateCount];
      
      newAutomaton->setStateCount(newStateCount);
      newAutomaton->setAlphabetSize(alphabetSize);
      newAutomaton->setAlphabet(alphabet);
      
      unsigned int newStateIdx = 0;
      for (int lhsStateIdx = 0; lhsStateIdx < this->stateCount; lhsStateIdx++) {
        for (int rhsStateIdx = 0; rhsStateIdx < rhs.getStateCount(); rhsStateIdx++) {
          char newName[100];
          newName[0] = '\0';
          strcat(newName, this->Q[lhsStateIdx].getName());
          strcat(newName, ",");
          strcat(newName, rhs.getQ()[rhsStateIdx].getName());
          
          q[newStateIdx].setName(newName);
          bool isNewStateFinite = this->Q[lhsStateIdx].getIsFinite() && rhs.getQ()[rhsStateIdx].getIsFinite();
          q[newStateIdx].setIsFinite(isNewStateFinite);
          newFiniteStatesCount += isNewStateFinite;
          
          q[newStateIdx].setIsInitial(this->Q[lhsStateIdx].getIsInitial() && rhs.getQ()[rhsStateIdx].getIsInitial());
          
          newStateIdx++;
        }
      }
      newAutomaton->setQ(q);
      newAutomaton->setFiniteStateCount(newFiniteStatesCount);
      
      newStateIdx = 0;
      for (int lhsStateIdx = 0; lhsStateIdx < this->stateCount; lhsStateIdx++) {
        for (int rhsStateIdx = 0; rhsStateIdx < rhs.getStateCount(); rhsStateIdx++) {
          newTransitionTable[newStateIdx] = new State[alphabetSize];
          for (int alphabetIdx = 0; alphabetIdx < alphabetSize; alphabetIdx++) {
            State lhsNext = this->transitionTable[lhsStateIdx][alphabetIdx];
            State rhsNext = rhs.getTransitionTable()[rhsStateIdx][alphabetIdx];
            char nextStateName[100];
            strcpy(nextStateName, lhsNext.getName());
            strcat(nextStateName, ",");
            strcat(nextStateName, rhsNext.getName());
            
            newTransitionTable[newStateIdx][alphabetIdx] = newAutomaton->getStateByName(nextStateName);
          }
          
          newStateIdx++;
        }
      }
      newAutomaton->setTransitionTable(newTransitionTable);
      
      return newAutomaton;
    }
    
    AbstractFiniteAutomaton *operator~() override {
      DeterministicFiniteAutomaton<T> *newAutomaton = this;
      
      for (int i = 0; i < stateCount; i++) {
        Q[i].setIsFinite(!Q[i].getIsFinite());
      }
      
      return newAutomaton;
    }
    
    std::ostream &print(std::ostream &out) const override {
      std::cout << "STATES COUNT: " << stateCount << std::endl;
      for (int i = 0; i < getStateCount(); ++i) {
        out << "Q[" << i << "] = " << getQ()[i].getName() << "\n";
      }
      out << "FiniteStateCount: " << getFiniteStatesCount() << "\n";
      out << "AlphabetSize: " << getAlphabetSize() << "\n";
      for (int i = 0; i < getStateCount(); ++i) {
        for (int j = 0; j < getAlphabetSize(); ++j) {
          out << "(" << Q[i].getName() << ")" << " -(" << alphabet[j] << ")> (" << getTransitionTable()[i][j].getName()
              << ")" << std::endl;
        }
        out << "\n";
      }
      out << "Finite states are: ";
      for (int i = 0; i < stateCount; i++) {
        if (Q[i].getIsFinite()) {
          out << Q[i].getName() << "\t";
        }
      }
      return out;
    }

private:
    State *Q;
    unsigned stateCount;
    unsigned finiteStatesCount;
    unsigned alphabetSize;
    State **transitionTable;
    T *alphabet;
};

#endif